export class DestinoViaje {

    private selected: boolean

    constructor(public nombre: String, public u: String) {
    }

    isSelected(): boolean {
        return this.selected
    }

    setSelected(s: boolean) {
        this.selected = s
    }
}