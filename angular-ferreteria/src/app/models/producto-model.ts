import { v4 as uuid } from 'uuid'

export class Producto {
    selected: boolean
    usos: string[]
    id = uuid()

    constructor(public nombre: String, public descripcion: String, public votes: number = 0) {
        this.usos = ['computadores', 'televisores']
    }

    isSelected(): boolean {
        return this.selected
    }

    setSelected(s: boolean) {
        this.selected = s
    }

    voteUp() {
        this.votes++
    }

    voteDown() {
        this.votes--
    }
}