import { Producto } from './producto-model'
import { Store } from '@ngrx/store'
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module'
import { AgregarProductoAction, ElegirProductoAction } from './productos-state.model'
import { forwardRef, Inject, Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http'

@Injectable()
export class ProductosApiClient {

    productos: Producto[] = []

    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient) {
        this.store
            .select(state => state.productos)
            .subscribe((data) => {
                this.productos = data.items
            })
    }

    add(p: Producto) {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' })
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: p.nombre }, { headers: headers })
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new AgregarProductoAction(p))
                const myDb = db
                myDb.productos.add(p)
                myDb.productos.toArray().then(productos => console.log(productos))
            }
        })
    }

    getById(id: string): Producto {
        return this.productos.filter(function (d) { return d.id.toString() === id })[0]
    }

    getAll(): Producto[] {
        return this.productos
    }

    elegir(p: Producto) {
        this.store.dispatch(new ElegirProductoAction(p))
    }
}