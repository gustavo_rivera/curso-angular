import {
    reducerProductos,
    ProductosState,
    initializeProductosState,
    InitMyDataAction,
    AgregarProductoAction
} from './productos-state.model';
import { Producto } from './producto-model';

describe('reducerProductos', () => {
    it('should reduce init data', () => {
        const prevState: ProductosState = initializeProductosState();
        const action: InitMyDataAction = new InitMyDataAction(['producto 1', 'producto 2']);
        const newState: ProductosState = reducerProductos(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('producto 1');
    });

    it('should reduce new item added', () => {
        const prevState: ProductosState = initializeProductosState();
        const action: AgregarProductoAction = new AgregarProductoAction(new Producto('Amplificador', 'url'));
        const newState: ProductosState = reducerProductos(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('Amplificador');
    });
});