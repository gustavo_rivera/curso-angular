import { Injectable } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { Action } from '@ngrx/store'
import { act, Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Producto } from './producto-model'

// Estado
export interface ProductosState {
    items: Producto[]
    loading: boolean
    favorito: Producto
}
export function initializeProductosState() {
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

//Acciones
export enum ProductosActionsTypes {
    AGREGAR_PRODUCTO = '[Productos] Agregar',
    RETIRAR_PRODUCTO = '[Productos] Retirar',
    ELEGIR_PRODUCTO = '[Productos] Elegir',
    VOTE_UP = '[Productos] Vote Up',
    VOTE_DOWN = '[Productos] Vote Down',
    INIT_MY_DATA = '[Productos] Init My Data'
}

export class AgregarProductoAction implements Action {
    type = ProductosActionsTypes.AGREGAR_PRODUCTO
    constructor(public producto: Producto) { }
}
export class RetirarProductoAction implements Action {
    type = ProductosActionsTypes.RETIRAR_PRODUCTO
    constructor(public producto: Producto) { }
}

export class ElegirProductoAction implements Action {
    type = ProductosActionsTypes.ELEGIR_PRODUCTO
    constructor(public producto: Producto) { }
}

export class VoteUpAction implements Action {
    type = ProductosActionsTypes.VOTE_UP
    constructor(public producto: Producto) { }
}

export class VoteDownAction implements Action {
    type = ProductosActionsTypes.VOTE_DOWN
    constructor(public producto: Producto) { }
}

export class InitMyDataAction implements Action {
    type = ProductosActionsTypes.INIT_MY_DATA
    constructor(public productos: string[]) { }
}

export type ProductosActions = AgregarProductoAction | RetirarProductoAction | ElegirProductoAction | VoteUpAction | VoteDownAction | InitMyDataAction

// Reducers
export function reducerProductos(state: ProductosState, action: ProductosActions): ProductosState {
    switch (action.type) {
        case ProductosActionsTypes.AGREGAR_PRODUCTO: {
            return {
                ...state,
                items: [...state.items, (action as AgregarProductoAction).producto]
            }
        }
        case ProductosActionsTypes.RETIRAR_PRODUCTO: {
            return {
                ...state,
                items: [...state.items, (action as RetirarProductoAction).producto]
            }
        }
        case ProductosActionsTypes.ELEGIR_PRODUCTO: {
            state.items.forEach(x => x.setSelected(false))
            const fav: Producto = (action as ElegirProductoAction).producto
            fav.setSelected(true)
            return {
                ...state,
                favorito: fav
            }
        }
        case ProductosActionsTypes.VOTE_UP: {
            const p: Producto = (action as VoteUpAction).producto
            p.voteUp()
            return { ...state }
        }
        case ProductosActionsTypes.VOTE_DOWN: {
            const p: Producto = (action as VoteDownAction).producto
            p.voteDown()
            return { ...state }
        }
        case ProductosActionsTypes.INIT_MY_DATA: {
            const productos: string[] = (action as InitMyDataAction).productos
            return {
                ...state,
                items: productos.map((p) => new Producto(p, ''))
            }
        }
    }
    return state
}

//Effects
@Injectable()
export class ProductosEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(ProductosActionsTypes.AGREGAR_PRODUCTO),
        map((action: AgregarProductoAction) => new ElegirProductoAction(action.producto))
    )
    constructor(private actions$: Actions) { }
}