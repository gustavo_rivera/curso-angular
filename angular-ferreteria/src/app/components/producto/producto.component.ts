import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { Producto } from '../../models/producto-model'
import { VoteUpAction, VoteDownAction } from '../../models/productos-state.model'

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class ProductoComponent implements OnInit {

  @Input() producto: Producto
  @Input("idx") position: number
  @HostBinding('attr.class') cssClass = 'col-md-4'
  @Output() clicked: EventEmitter<Producto>

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter()
  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.producto)
    return false
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.producto))
    return false
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.producto))
    return false
  }
}
