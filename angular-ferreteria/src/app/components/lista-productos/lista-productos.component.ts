import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { Producto } from '../../models/producto-model'
import { ProductosApiClient } from '../../models/productos-api-client.model'

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css'],
  providers: [ProductosApiClient]
})
export class ListaProductosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<Producto>
  updates: string[]
  all

  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(public productosApiClient: ProductosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter()
    this.updates = []
    this.store.select(state => state.productos.favorito)
      .subscribe(p => {
        if (p != null) {
          this.updates.push('Se ha elegido a ' + p.nombre)
        }
      })
    store.select(state => state.productos.items).subscribe(items => this.all = items)
  }

  ngOnInit(): void {
  }

  agregado(p: Producto) {
    this.productosApiClient.add(p)
    this.onItemAdded.emit(p)
  }

  elegido(p: Producto) {
    this.productosApiClient.elegir(p)
  }

  retirado(p: Producto) {

  }

  getAll() { }
}
