import { Component, OnInit, Output, EventEmitter, Injectable, forwardRef, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';
import { Producto } from '../../models/producto-model';

@Component({
  selector: 'app-form-producto',
  templateUrl: './form-producto.component.html',
  styleUrls: ['./form-producto.component.css']
})
export class FormProductoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Producto>
  fg: FormGroup
  minLongitud = 3
  searchResults: string[]

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter()
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      desc: ['']
    })
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre')
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/productos?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response)
  }

  guardar(nombre: string, desc: string): boolean {
    let p = new Producto(nombre, desc)
    this.onItemAdded.emit(p)
    return false
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    let l = control.value.toString().trim().length
    if (l > 0 && l < 5) {
      return { invalidNombre: true }
    }
    return null
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length
      if (l > 0 && l < minLong) {
        return { minLongNombre: true }
      }
      return null
    }
  }
}
