import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { ProductosActionsTypes } from 'src/app/models/productos-state.model';
import { Producto } from '../../models/producto-model';
import { ProductosApiClient } from '../../models/productos-api-client.model';
/*
export class ProductosApiClientViejo {
  getById(id: string): Producto {
    console.log('llamando por la clase vieja')
    return null
  }
}

interface AppConfig {
  apiEndpoint: string
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi-api.com'
}

const APP_CONFIG = new InjectionToken<AppConfig>('app.config')

@Injectable()
class ProductosApiClientDecorated extends ProductosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store)
  }
  getById(id: string): Producto {
    console.log('llamando por la clase decorada!')
    console.log('config: ' + this.config.apiEndpoint)
    //return super.getById(id)
    return null
  }
}
*/
@Component({
  selector: 'app-producto-detalle',
  templateUrl: './producto-detalle.component.html',
  styleUrls: ['./producto-detalle.component.css'],
  providers: [ProductosApiClient]
  /*    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
      { provide: ProductosApiClient, useClass: ProductosApiClientDecorated },
      { provide: ProductosApiClientViejo, useExisting: ProductosApiClient }
    ]*/
})
export class ProductoDetalleComponent implements OnInit {
  producto: Producto

  constructor(private route: ActivatedRoute, private productosApiClient: ProductosApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    //this.producto = this.productosApiClient.getById(id)
    this.producto = null
  }

}
