import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-ferreteria';

  constructor(public translate: TranslateService) {
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)))
    translate.setDefaultLang('es')
  }
}
